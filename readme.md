# COMP 4750 - Assignment 2

Created By: Devin Marsh

### Usage

You can run this script in only two ways:

##### Reconstruct Surface Strings

`reconstruct.py surface LEXICAL_FILE F1 ... Fn`

##### Reconstruct Lexical Strings

`reconstruct.py lexical SURFACE_FILE F1 ... Fn`

---
* _LEXICAL_FILE_ is the file containing a list of lexical strings (one per line).
* _SURFACE_FILE_ is the file containing a list of surface strings (one per line).
* _F1_ is the file describing the Finite State Transducer (FST). You may provide multiple FST
files; they will get composed together.

### Description

reconstruct.py will take a form strings (either surface or lexical) and reconstruct
the other form's possible strings.