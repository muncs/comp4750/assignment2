#########################################################
##  CS 4750 (Fall 2018), Assignment #2                 ##
##   Script File Name: fstFunctions.py                 ##
##       Student Name: Devin Marsh                     ##
##         Login Name: dfcm15                          ##
##              MUN #: 201239464                       ##
#########################################################

"""
Contains functions related to Finite State Transducers (FST).
"""

# Constants
# Keys to access fields within a FST
KEY_STATES = "states"
KEY_META = "meta"
KEY_FINAL = "final"
KEY_NUM_STATES = "numStates"
KEY_CHARACTERS = "characters"
KEY_TRANSITIONS = "transitions"
KEY_INITIAL_STATE = "1"


def readFST(filename):
    """
    Parse and return a FST from filename
    :param filename: Input file containing FST information
    :return: FST
    """

    fst = {KEY_META: {}, KEY_STATES: {}}

    with open(filename) as input_file:
        current_state = -1

        for line in input_file:
            words = line.split(" ")

            # Strip newline character
            for i in range(len(words)):
                words[i] = words[i].rstrip("\n")

            # Read FST metadata
            if not fst[KEY_META]:
                fst[KEY_META][KEY_NUM_STATES] = words[0]
                fst[KEY_META][KEY_CHARACTERS] = words[1]
            else:
                # Check to see if new state or transition definition
                if not words[0] == "":
                    current_state = words[0]
                    fst[KEY_STATES][current_state] = {
                        KEY_FINAL: True if words[1] == "F" else False,
                        KEY_TRANSITIONS: {}}
                else:
                    transitions = fst[KEY_STATES][current_state][KEY_TRANSITIONS]
                    tran_key = words[2] + ":" + words[3]
                    if tran_key in transitions:
                        transitions[tran_key].add(words[4])
                    else:
                        transitions[tran_key] = {words[4]}

    return fst


def composeFST(f1, f2):
    """
    Compose two FSTs together.
    :param f1: First FST
    :param f2: Second FST
    :return: Composed FST from f1 and f2
    """

    fst = {KEY_META: {}, KEY_STATES: {}}

    f1_current_state, f2_current_state = KEY_INITIAL_STATE, KEY_INITIAL_STATE       # Keep track of each FST's current state
    state_queue = [f1_current_state + "," + f2_current_state]                       # Queue for composed states to be processed in. Add initial state to be processed
    state_map = {}                                                                  # Map of composed state names to unit state names
    processed_states = set()

    language_set = set()

    # Process queue
    while state_queue:
        pair = state_queue[0].split(",")
        f1_current_state, f2_current_state, current_state = pair[0], pair[1], pair[0] + "," + pair[1]

        # Check to see if state is already processed
        if current_state in processed_states:
            del state_queue[0]
            continue

        # Add composed state name to map
        if current_state not in state_map:
            state_map[current_state] = str(len(state_map) + 1)

        # Iterate over every f1 transition
        for f1_transition, f1_states in f1[KEY_STATES][f1_current_state][KEY_TRANSITIONS].iteritems():
            pair = f1_transition.split(":")
            input1, output1 = pair[0], pair[1]

            # Iterate over every f2 transition
            for f2_transition, f2_states in f2[KEY_STATES][f2_current_state][KEY_TRANSITIONS].iteritems():
                pair = f2_transition.split(":")
                input2, output2 = pair[0], pair[1]

                # If the output of f1 equals input of f2, make a new state
                if output1 == input2:
                    tran_key, state_key = input1 + ":" + output2, list(f1_states)[0] + "," + list(f2_states)[0]

                    # Add to character domain
                    language_set.add(input1)
                    language_set.add(output2)

                    # Build subdictionary
                    if current_state not in fst[KEY_STATES]:
                        fst[KEY_STATES][current_state] = {KEY_TRANSITIONS: {}, KEY_FINAL: False}

                    # Add next composed state
                    fst[KEY_STATES][current_state][KEY_TRANSITIONS][tran_key] = {state_key}

                    # Add next composed state to queue to be process
                    if state_key not in state_queue:
                        state_queue.append(state_key)

                # Add final state
                if f1[KEY_STATES][list(f1_states)[0]][KEY_FINAL] and f2[KEY_STATES][list(f2_states)[0]][KEY_FINAL] and state_key not in fst[KEY_STATES].keys():
                    fst[KEY_STATES][state_key] = {KEY_TRANSITIONS: {}, KEY_FINAL: True}

        # Do not process again
        processed_states.add(state_queue[0])

        # Pop the queue item
        state_queue.pop(0)

    # Meta
    fst[KEY_META][KEY_NUM_STATES] = len(fst[KEY_STATES])
    fst[KEY_META][KEY_CHARACTERS] = ""

    # Process character domain
    for character in language_set:
        fst[KEY_META][KEY_CHARACTERS] += character

    # Refactor composed states and return FST
    return __refactorComposedStates(state_map, fst)


def reconstructUpper(lower, fst):
    """
    Print the set of upper strings associated with a string
    :param lower: String containing lower symbols
    :param fst: FST to process
    :return:
    """

    __reconstructUpper(lower, "", KEY_INITIAL_STATE, fst)


def __reconstructUpper(lower, upper, state, fst):
    """
    Print the set of upper strings associated with a string
    :param lower: String containing lower symbols
    :param fst: FST to process
    :return:
    """

    lower = lower.strip(" ")    # Strip whitespace, allowing strings with spaces to be reconstructed
    if not lower:
        if fst[KEY_STATES][state][KEY_FINAL]:
            print upper
        return
    else:
        for transition, states in fst[KEY_STATES][state][KEY_TRANSITIONS].iteritems():
            pair = transition.split(":")
            x, y = pair[0], pair[1]
            if lower == x + lower[len(x):]:
                for next_state in states:
                    __reconstructUpper(lower[len(x):], upper + y, next_state, fst)


def reconstructLower(upper, fst):
    """
    Print the set of lower strings associated with a string
    :param upper: String containing upper symbols
    :param fst: FST to process
    :return:
    """

    __reconstructLower(upper.strip(" "), "", KEY_INITIAL_STATE, fst)


def __reconstructLower(upper, lower, state, fst):
    """
    Print the set of lower strings associated with a string
    :param upper: String containing upper symbols
    :param fst: FST to process
    :return:
    """

    upper = upper.strip(" ")    # Strip whitespace, allowing strings with spaces to be reconstructed
    if not upper:
        if fst[KEY_STATES][state][KEY_FINAL]:
            print lower
        return
    else:
        for transition, states in fst[KEY_STATES][state][KEY_TRANSITIONS].iteritems():
            pair = transition.split(":")
            x, y = pair[0], pair[1]
            if upper == y + upper[len(y):]:
                for next_state in states:
                    __reconstructLower(upper[len(y):], lower + x, next_state, fst)


def __refactorComposedStates(state_map, fst):
    """
    Refactor composed state names ({1,1}, {2,4}, etc) as unit states ({0}, {1}, etc)
    :param state_map: Map of composed state names to unit state names
    :param fst: FST to process
    :return: FST with no composed state names
    """

    processed_states = set()

    # Iterate over every state and refactor composed state names to their unit state names
    for state in fst[KEY_STATES]:
        if state not in processed_states:
            for transitionKey, nextState in fst[KEY_STATES][state][KEY_TRANSITIONS].iteritems():
                    refactored_states = set()
                    for _state in nextState:
                        refactored_state = state_map[_state]
                        refactored_states.add(refactored_state)
                    fst[KEY_STATES][state][KEY_TRANSITIONS][transitionKey] = refactored_states

            refactored_state = state_map[state]
            fst[KEY_STATES][refactored_state] = fst[KEY_STATES].pop(state)
            processed_states.add(refactored_state)

    return fst
