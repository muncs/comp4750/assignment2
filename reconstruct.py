#########################################################
##  CS 4750 (Fall 2018), Assignment #2                 ##
##   Script File Name: reconstruct.py                  ##
##       Student Name: Devin Marsh                     ##
##         Login Name: dfcm15                          ##
##              MUN #: 201239464                       ##
#########################################################

"""
Script to reconstruct
"""

# Imports
import sys
import fstFunctions

# Constants
MIN_ARGS = 4
FORM_SURFACE = "surface"
FORM_LEXICAL = "lexical"


# Function declarations
def __readFormFile(filename):
    """
    Read in the form (surface or lexical) file.
    :param filename: File to read
    :return: Array of form strings
    """
    form_strings = []

    with open(filename) as input_file:
        for line in input_file:
            form_strings.append(line.strip("\n"))

    return form_strings


def __printFstInfo(fst):
    """
    Print information about the FST
    :param fst:
    :return:
    """
    num_states = len(fst[fstFunctions.KEY_STATES])
    num_transitions = 0

    for state in fst[fstFunctions.KEY_STATES]:
        num_transitions += len(fst[fstFunctions.KEY_STATES][state][fstFunctions.KEY_TRANSITIONS])

    print "FST has %d states and %d transitions" % (num_states, num_transitions)


"""BEGIN PROGRAM"""

# Get command arguments
args = sys.argv

# Warn user if there is less than required number of args
if len(args) < MIN_ARGS:
    sys.exit("Missing program arguments. Syntax [surface|lexical] [wlf|wsf] F1 ... Fn")

# Parse form arguments
form_type = args[1]
form_strings = __readFormFile(args[2])

# Parse FST arguments
fst_list = []
for file_name in args[3:]:
    fst_list.append(fstFunctions.readFST(file_name))

fst = fst_list[0]

# Compose all FSTs together if there is more than one
if len(fst_list) > 1:
    for i in range(0, len(fst_list) - 1):
        fst = fstFunctions.composeFST(fst_list[i], fst_list[i+1])

# Print FST information
__printFstInfo(fst)

# Reconstruct relative form
if form_type == FORM_SURFACE:
    for lower_string in form_strings:
        print "Lexical form: " + lower_string
        print "Reconstructed surface forms:"
        fstFunctions.reconstructUpper(lower_string, fst)
        print "-----------------------------"
elif form_type == FORM_LEXICAL:
    for upper_string in form_strings:
        print "Surface form: " + upper_string
        print "Reconstructed lexical forms:"
        fstFunctions.reconstructLower(upper_string, fst)
        print "-----------------------------"
else:
    sys.exit("Unable to determine form (surface or lexical). Exiting...")
